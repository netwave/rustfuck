#![allow(non_snake_case)]
use std::collections::vec_deque as queue;
use std::io::prelude::*;
use std::io;
use std::fs::File;
use std::env;
use std::str::Chars;

#[derive(Debug)]
struct CellData 
{
    cells   : queue::VecDeque<u8>,
    index   : usize
}

impl CellData {
    
    pub fn new() -> CellData {
        let mut vecqueue = queue::VecDeque::with_capacity(256);
        for _ in 0..256 {
            vecqueue.push_back(0);
        }
        CellData {cells : vecqueue, index : 127}
    }

    pub fn next(&mut self)
    {
        self.index += 1;
        if self.index >= self.cells.len() 
        {
            self.cells.push_back(0u8);
        }
    }

    pub fn prev(&mut self)
    {
        if self.index == 0
        {
            self.cells.push_front(0u8);
        }
        else 
        {
            self.index -= 1;
        }
    }

    pub fn inc(&mut self)
    {
        let val = self.cells[self.index];
        if val == 255 {
            self.cells[self.index] = 0;
        }
        else {
            self.cells[self.index] += 1;
        }
    }

    pub fn dec(&mut self)
    {
        if self.getVal() == 0 {
            self.cells[self.index] = 255;
        }
        else {
            self.cells[self.index] -= 1;
        }
    }

    fn getVal(&self) -> u8
    {
        self.cells[self.index]
    }

    pub fn printVal(&self)
    {
        print!("{}", self.getVal() as char);
    }

    pub fn getInput(&mut self)
    {
        let mut buf = String::new();
        match io::stdin().read_line(&mut buf) {
            Ok(_) => {
                if let Ok(num) = buf.parse::<u8>() {
                    self.cells[self.index] = num;
                }
            }
            Err(error) => println!("error: {}", error)
        }
    }

    pub fn operate(&mut self, c: &char)
    {
        match c {
            &'>' => self.next    (),
            &'<' => self.prev    (),
            &'+' => self.inc     (),
            &'-' => self.dec     (),
            &'.' => self.printVal(),
            &',' => self.getInput(),
            _    => ()
        }
    }
}

fn eval_loop(celldata : &mut CellData, iter: &mut Chars)
{
    'outer : loop {
        let mut it = iter.clone();
        'inner : while let Some(c) = it.next() {
            match c {
                '[' => eval_loop(celldata, &mut it),
                ']' => {
                    match celldata.getVal() {
                        0u8 => {*iter = it; break 'outer; }
                        _   => {break 'inner;}
                    }
                },
                _   => celldata.operate(&c)
            }
        }
    }
}

fn main_eval(celldata : &mut CellData, iter: &mut Chars)
{
    while let Some(c) = iter.next() {
        match c {
            '[' => eval_loop(celldata, iter),
            _   => celldata.operate(&c)
        }
    }
}

fn main() {
    let mut the_args = env::args();
    the_args.next(); //skip self program name
    if let Some(filename) = the_args.next()
    {
        let mut file = File::open(filename).expect("File not found");
        let mut buff = String::new();
        file.read_to_string(&mut buff)
            .expect("Could not properly read file");
        let orders : String = buff.chars().filter(|c :& char| {
            match c {
                &'>' | &'<' | &'+' | &'-' | &'.' | &',' | &'[' | &']'
                    => true,
                _   => false
            }
        }).collect();
        let mut celldata = CellData::new();
        let mut it       = orders.chars();
        main_eval(&mut celldata, &mut it);
    }
}
